# Ejemplo curso Udemy Vue 3

- [Preview page](https://salcar.netlify.app/)

Este es un ejemplo del curso de Udemy de vue.js 3, trabajando con componentes y el Composite API.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
